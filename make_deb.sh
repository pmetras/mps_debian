#!/bin/bash
#
# Create MyPhotosShare Debian/Ubuntu package from GitLab
# Author: Pierre Métras

# Defaults
##########

# TRANSPILE=N --> Use uglifyjs.terser
# TRANSPILE=Y --> Use buble + uglifyjs (does not work with MyPhotoShare v4.9+)
TRANSPILE=N

# Get parameters
################

OVERWRITE=N
DOWNLOAD=Y

while getopts "od" option; do
	case $option in
		o)
			OVERWRITE=Y
		;;
		d)
			DOWNLOAD=N
		;;
		\?)
			echo "Usage: $0 [-o | -d] VERSION_TAG"
			echo "	-o: overwrite if MyPhotoShare directory already exists"
			echo "	-d: don't download MyPhotoShare from GitLab and use existing tar.gz file"
			echo "The VERSION_TAG is a release version tag from https://gitlab.com/paolobenve/myphotoshare/tags"
			echo "Version must be >=3.4 to generate documentation"
			echo "Example: $0 v3.5"
			echo
			echo "You must adapt files into 'debian' directory to the version you want to package"
			echo "else the build will fail or will be invalid for distribution."
			exit 1
		;;
	esac
done
shift $((OPTIND-1))

VERSION="$1"
if [ -z "${VERSION}" ]; then
	echo "Missing MyPhotoShare release version"
	echo "Check https://gitlab.com/paolobenve/myphotoshare/tags for version tags,"
	echo "with the pattern vY.Z where Y.Z is the version number."
	echo "Use 'vY.Z' as argument for $0."
	exit 1
fi


# Check that debian/* package files have been adapted
#####################################################
# Ideally, the 'changelog' file should be automatically updated with the content of
# GitLab tag message...

VERSION_LOG="$(sed -nr -e '1,1s/^myphotoshare\s*\((.+)\)\s*.*$/\1/p' debian/changelog)"

if [ "v${VERSION_LOG%-*}" != "${VERSION}" ]; then
	echo "Files in 'debian' directory are for MyPhotoShare version '${VERSION_LOG}'."
	echo "You must adapt files into 'debian' directory to the version you want to package"
	echo "else the build will fail or will be invalid for distribution."
	echo "Hint: look at 'debian/changelog' first..."
	exit 1
fi


# Check dependencies to build package
#####################################

# cssmin is used to build minified CSS. I don't know what is Debian standard.
cssmin -h > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "'cssmin' is not installed. Look for package 'cssmin' or 'https://github.com/zacharyvoase/cssmin'"
	exit 1
fi

# uglifyjs or uglifyjs.terser is used to build minified JavaScript in Debian
if [ "${TRANSPILE}" = "Y" ]; then
	uglifyjs -V > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		echo "'uglifyjs' is not installed. Look for package 'node-uglifyjs' or 'http://lisperator.net/uglifyjs/'"
		exit 1
	fi
else
	uglifyjs.terser -V > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		echo "'uglifyjs.terser' is not installed. Look for package 'uglifyjs.terser' or 'https://terser.org/'"
		exit 1
	fi
fi

# buble is used to convert ES6 to ES5 JavaScript version
if [ "${TRANSPILE}" = "Y" ]; then
	buble -v > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		echo "'buble' is not installed. Look for package 'node-buble' or 'https://github.com/Rich-Harris/buble'"
		exit 1
	fi
fi

# wget to download MyPhotoShare sources from GitLab.
wget -V > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "'wget' is not installed. Look for package 'wget'"
	exit 1
fi

# pandoc is used to build documentation
pandoc -v > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "'pandoc' is not installed. Look for package 'pandoc' or 'https://pandoc.org/'"
	exit 1
fi

# Now we go!
############

TARBALL="myphotoshare_${VERSION/v/}.orig.tar.gz"

# Now we have more consistent release names
URL="https://gitlab.com/paolobenve/myphotoshare/-/archive/${VERSION}/myphotoshare-${VERSION}.tar.gz"

DIRECTORY="myphotoshare-${VERSION/v/}"


# Download sources from GitLab
##############################

if [ "${DOWNLOAD}" = "Y" ]; then
	wget --progress=dot:mega -O "${TARBALL}" "${URL}"
	if [ $? -ne 0 ]; then
		echo "Can't download MyPhotoShare source from '${URL}'"
		exit 1
	fi
else
	if [ ! -e ${TARBALL} ]; then
		echo "Can't find file '${TARBALL}'"
		exit 1
	fi
fi

if [ "$OVERWRITE" = "Y" ]; then
	echo "Using existing '${DIRECTORY}'"
else
	if [ -d "${DIRECTORY}" ]; then
		rm -rf "${DIRECTORY}"
	fi
fi

echo "Extracting MyPhotoShare from '${TARBALL}'"
tar -xf "${TARBALL}"
if [ $? -ne 0 ]; then
	echo "Tar file '${TARBALL}' can't be extracted."
	exit 1
fi
mv -f "myphotoshare-${VERSION}" "${DIRECTORY}"
if [ $? -ne 0 ]; then
	echo "Haven't found myphotoshare-${VERSION} directory into ${TARBALL}."
	exit 1
fi


# Prepare debian directory
##########################

cp -r debian "${DIRECTORY}/"

cd "${DIRECTORY}/"


# Minify sources
################

# Old Debian-approved method...
#pushd web/css
#rm -f *.min.css
#ls -1 *.css | grep -Ev "min.css$" | while read cssfile; do
#	newfile="${cssfile%.*}.min.css"
#	echo "Minifying '${cssfile}'"
#	cssmin < ${cssfile} > ${newfile}
#done
#rm -f styles.min.css
#cat *.min.css > styles.min.css
#popd
#
#pushd web/js
#rm -f *.min.js
#ls -1 *.js | grep -Ev "min.js$" | while read jsfile; do
#	if [ "${TRANSPILE}" == "Y" ]; then
#		newfile="${jsfile%.*}.min.js"
#		echo "Minifying '${jsfile}' to '${newfile}'"
#		buble ${jsfile} | uglifyjs - -o ${newfile}
#	else
#		newfile="${jsfile%.*}.min.js"
#		echo "Minifying '${jsfile}' to '${newfile}'"
#		uglifyjs.terser -o ${newfile} ${jsfile}
#	fi
#done
#rm -f scripts.min.js
#cat *.min.js > scripts.min.js
#popd

# New method using MyPhotoShare minifiers
# This must be used as MyPhotoShare produces multiple result files depending
# on options.
./bin/js-css-minify.sh

# Create documentation
######################

if [ -d doc ]; then
	pushd doc
	sed -e '1,$s#doc/img/#img/#' ../README.md | pandoc -s -f gfm -t html5 --metadata pagetitle="Read Me" -o index.html
	sed -e '1,$s#doc/img/#img/#' ../README.md | pandoc -s -f gfm -t plain --metadata pagetitle="Read Me" -o README.txt
	ls -1 *.md | while read mdfile; do
		newfile="${mdfile%.*}.html"
		echo "Creating documentation '${newfile}'"
		pandoc -s -f gfm -t html5 --metadata pagetitle="${mdfile/.md}" -o ${newfile} ${mdfile}
	done
	ls -1 *.md | while read mdfile; do
		newfile="${mdfile%.*}.txt"
		echo "Creating documentation '${newfile}'"
		pandoc -s -f gfm -t plain --metadata pagetitle="${mdfile/.md}" -o ${newfile} ${mdfile}
	done
	popd
fi


# The rest of configuration is magic in configuration files in debian/*
#######################################################################


# Create Debian/Ubuntu package
##############################

dpkg-buildpackage -us -uc -A

