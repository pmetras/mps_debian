# Create Debian/Ubuntu package for MyPhotoShare

[MyPhotoShare](https://gitlab.com/paolobenve/myphotoshare) is a static photo and video gallery. This script creates a Debian/Ubuntu package from MyPhotoShare releases.

## What are the requirements?

A Debian or Ubuntu distribution and to know how to run a shell script from the command line.

## How to use it?

Run `$ ./make_deb.sh MYPHOTOSHARE_VERSION` and wait while the script builds a fresh package. The `MYPHOTOSHARE_VERSION` parameter must be a [recent version](https://gitlab.com/paolobenve/myphotoshare/releases) or you'll have to adapt `debian/control`, `debian/changelog` and probably other files of the `debian` directory.

If you don't want to download the `tar.gz` file from GitLab because this file is already present in the directory, you can use option `-d`.

## Can you give an example?

Here it is:

```
$ ./make_deb.sh v3.4beta3
--2018-03-09 21:15:54--  https://gitlab.com/paolobenve/myphotoshare/archive/v3.4beta3.tar.gz
Resolving gitlab.com (gitlab.com)... 192.30.253.112, 192.30.253.113
Connecting to gitlab.com (gitlab.com)|192.30.253.112|:443... connected.
HTTP request sent, awaiting response... 302 Found
Location: https://codeload.gitlab.com/paolobenve/myphotoshare/tar.gz/v3.4beta3 [following]

...

dpkg-deb: building package 'myphotoshare' in '../myphotoshare_3.4beta3-1_all.deb'.
 dpkg-genchanges -A >../myphotoshare_3.4beta3-1_all.changes
dpkg-genchanges: binary-only arch-indep upload (source code and arch-specific packages not included)
 dpkg-source --after-build myphotoshare-3.4beta3
dpkg-buildpackage: binary-only upload (no source included)
```

## Do you provide resulting Debian packages?

Usually no. You have to build it yourself. It generates the `DEB` package in less than one minute... And the Debian package is larger than 10 MB while the script sources are a few KB.

But for major versions, I can create a release and attach the resulting binary `.deb` package. These packages are provided so that you can test easily and evaluate when you do an update. I won't update the release for minor versions. So if you want to get the latest bug fixes, build your own packages.

## How does it work?

The creation of the package is a bit unorthodox as it downloads the sources of a [recent release from GitLab](https://gitlab.com/paolobenve/myphotoshare/releases) and creates the package from them, while the `debian` directory origins from outside of the project directory.

## Are there bugs?

Probably. The major limitation is that the script works well with the latest version of MyPhotoShare, currently 5.0. The `debian` content has to be adapted when new versions of MyPhotoShare are released. Make sure that 'debian/changelog' knows about the version you want to package.

## Has it been extensively tested?

No. Use at you own risks. You can create [GitLab Issues](https://gitlab.com/pmetras/mps_debian/issues) if you encounter problems or you want to submit patches. I'll try to solve them if I've time.

It is known to run on Debian 9 and 10 and Ubuntu 16.04, 18.04 and 20.04 LTS versions.

## I've created the Debian package. What do I do now?

You install it with the command `$ sudo dpkg -i myphotoshare_X.Y.Z-T_all.deb` where `X`, `Y`, `Z` and `T` are version, sub-version, release and Debian release numbers.

Notice that you need to have administrative credentials to run `sudo`.

## Where do I put the photos and videos?

I did not found where Debian recommends storing media for galleries, so I decided to use `/usr/local/share/media`.

You can change it with a more verbose `debconf` level. You can set it with `# dpkg-reconfigure debconf` and then `# dpkg-reconfigure myphotoshare`.

## Why the user commands in the resulting package are not the same as the ones documented in MyPhotoShare?

To avoid name clashes with other packages, I've renamed the original MyPhotoShare commands.

 * `myphotoshare_scanner` is `bin/scanner` or `scanner/main.py`.
 * `myphotoshare_get_alternate_names` is `bin/get_alternate_names.py`.
 * `myphotoshare_js_css_minify` is `bin/js-css-minify.sh`.
 * `myphotoshare_make_album_ini` is `bin/make_album_ini.sh`.

These command must be run impersonating user `myphotoshare`. You can do it with `$ sudo su -c "command..." myphotoshare`.

Read the documentation in `/usr/share/doc/myphotoshare` or `man myphotoshare` to learn more about MyPhotoShare and the changes done.

## Why is the cronjob failing with 'ascii codec error' or locale error?

A cronjob scans daily the `album` folder and indexes new content. This cronjob runs as user `myphotoshare` with a limited environment. Particularly, cronjobs usually run with `C` locale that supports only `ASCII`. You need to change that environment with a `UTF-8` locale supported on your server. First, you need to find which locales are available on your computer with:

```bash
$ sudo su
# locale -a
```

Then edit `/etc/defaults/myphotoshare` to use one of the locale listed corresponding to your language. By default, it assumes `en_US.UTF-8`.

## Can I prevent an upgrade from clearing the cache?

There could be a bug in the package that empties the cache when the package is upgraded. I don't know for sure as I always empty my cache folder, as there was a bug in MyPhotoShare cache management a log time ago... But as MyPhotoShare is now more smart about when it must refresh the cache, you can keep its content between upgrades. While the *empty cache* bug could be there, you can prevent the package from clearing it by setting a value to the `MPS_KEEPCACHE` variable. For instance, use the following to upgrade without cache reset:

```bash
$ sudo MPS_KEEPCACHE=1 dpkg -i myphotoshare_X.Y.Z-T_all.deb
```

## Anything else to add?

There's a cronjob that updates MyPhotoShare cache daily. You only have to add media albums and files into `/usr/local/share/media` to have them published on your web gallery. Look at the result by opening a web browser at [http://localhost/myphotoshare](http://localhost/myphotoshare).

You can also refresh the cache manually with `$ sudo su -c "myphotoshare_scanner /etc/myphotoshare/myphotoshare.conf" myphotoshare`.

Or you can change the scanner to run weekly or monthly if you don't add new pictures frequently, or if you have a lot of pictures.

Don't forget that for all commands but `myphotoshare_make_album_ini`, you must change user to `myphotoshare`.

Of course, documentation is available in `/usr/share/doc/myphotoshare`.

Enjoy!


## What's the license?

This script is public domain. It was originally written by Pierre Métras <pierre@alterna.tv>, but you are free to do whatever you want from it.
